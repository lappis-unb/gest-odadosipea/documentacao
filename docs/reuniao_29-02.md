# Reunião IPEA 29/02
Reunião realizada presencialmente no IPEA junto a Coordenação-Geral de Gestão Estratégica e Orçamento (CGPGO).  
A reunião contou com os responsáveis pelo planejamento orçamentário e com os responsáveis pela execução orçamentária/financeira.

## Resumo

### Planejamento orçamentário - SIOP
O ponto de partida para o orçamento é a Lei orçamentária (LOA), ela é elaborada no ano anterior e conta com todos os gastos do governo federal.  
A LOA define o orçamento do órgão, chamado de dotação inicial. Após a aprovação da LOA pelo congresso a *Dotação Inicial* só pode ser alterada por alguma outra lei, e quando isso ocorre ela é atualizada e passa a ser chamada de *Dotação Atualizada*.

Depois de aprovada, a LOA é então publicada, gerando o documento de Nota de Dotação.

O planejamento orçamentário é realizado no SIOP e é separado em 2 tipos: 
1. Orçamento Qualitativo
    O orçamento qualitativo é onde são descritos os programas e as ações. Nele também é definido como as ações serão realizadas, o que será produzido, os responsáveis e os beneficiados.
2. Orçamento Quantitativo
    O orçamento quantitativo define para cada ação orçamentária os elementos de despesa, a fonte de recursos e a dotação.

Após esse processo, as dotações para cada ação estão definidas e o planejamento está finalizado podendo assim se iniciar a fase de execução.

As dotações representam o comprometimento daquele montante para uma determinada ação, assim, o órgão consegue saber quanto da dotação inicial já foi comprometida.

#### Termo de execução descentralizada - TED
Durante o processo de planejamento podem ocorrer as TEDs, elas são uma via de mão dupla, onde o órgão pode receber TEDs ou enviar TEDs.  
No momento do recebimento/envio das TEDs o valor do orçamento é repassado por meio de nota de crédito pelo TransfereGov e é gerado um documento de disponibilidade orçamentária. **?1**  
Esse valor precisa ser empenhado pelo órgão executor da TED e é responsabilidade do órgão concedente acompanhar esse processo.  
Após ser empenhado pelo órgão executor, o valor da TED é abatido na dotação do órgão concedente.  

**?2**

### Execução orçamentária - SIAFI
A execução parte das dotações definidas no planejamento e basicamente é dividida em 3 etapas: empenho, liquidação e pagamento.

A primeira etapa da execução é o empenho. O empenho é o compromisso de pagamento de um valor para um favorecido.  
Para cada ação do planejamento existe uma dotação, quando um valor é empenhado, é gerada uma NE que é abatida da dotação.  
Os valores empenhados podem estar vinculados a licitações, TEDs, convênios, passagens e diárias.  
- As licitações são realizadas por meio do ComprasNet, onde é gerada uma dispensa que é vinculada a uma NE. **?3**
- As TEDs e convênios são realizadas pelo TransfereGov, e a NE é gerada pelo órgão executor.
- As passagens e diárias são gerenciadas pelo SCDP, porém, antes de realizar o cadastro da PCDP no SCDP é necessário que o valor seja empenhado no SIAFI.

A segunda etapa é a liquidação. É a etapa em que se faz a verificação da prestação de serviços, entrega de bens ou realização de obras pactuados durante a etapa de empenho.  
Nessa etapa, é gerada a nota de lançamento.

A terceira etapa é referente ao pagamento da despesa em si. **?4**

### Geração de relatórios
A maioria dos relatórios é gerada pelo Tesouro Gerencial, porém, eles usam muitos termos técnicos que dificulta o entendimento pelos diretores e pela presidencia.  
Pra contornar essa situação a Beatriz extrai esses dados do TG e monta os relatórios em Excel.  
Os principais relatórios gerados são:
- Total de orçamento
- Quanto foi executado/quanto ainda tem disponível
- Total de financeiro
- Entradas e saídas de TEDs

## Principais problemas identificados
- Tesouro Gerencial é utilizado para extrair os dados do SIAFI, porém, os dados só são extraídos durante a noite e só ficam disponíveis no dia seguinte
- A diretoria e presidência tem dificuldades para entender os termos técnicos dos relatórios gerados pelo sistema
- Dificuldade no controle/visualização de quanto já foi comprometido da dotação inicial
- Os valores enviados via TED geram uma NC e precisam ser emprenhados pelo favorecido **?5** **?6**
- Diferenças entre financeiro e orçamento
  - Não foi falado muito sobre, porém acredito que deva ser um ponto de atenção

### Dúvidas
1. Esse valor é acrescido à dotação do órgão?
2. As TEDs ocorrem na etapa de plajamento ou execução?
3. Qual identificador do ComprasNet é utilizado para esse vinculo?
4. Na etapa de pagamento foi comentado sobre o GERCOMP e o GEROP, onde esses 2 se encaixam no processo?
5. O doc de disponibilidade orçamentária é gerado junto com a NC?
6. Qual a diferença entre nota de dotação e documento de disponibilidade orçamentária?

## Léxico
1. LOA - Lei orçamentária  
2. ND - Nota de dotação  
3. NC - Nota de crédito  
4. NE - Nota de empenho
5. NL/NS - Nota de lançamento/Nota de lançamento do sistema
6. PF - Programação financeira 
   1. Quanto o órgão vai gastar em um determinado período de tempo
7. Empenhado - Compromisso de pagamento
8. Liquidado - Momento da aquisição de bens ou contratação de serviços
9. Pago - Momento em que ocorre o pagamento
10. Orçamento - Planejamento
11. Financeiro - Execução do pagamento
12. TED - Termo de execução descentralizada
13. PCDP - Proposta de concessão de diárias e passagens

## Sistemas utilizados
- SIOP (Planejamento e Orçamento)
  - Qualitativo (Programas e ações)
  - Quantitativo (Dotação para cada ação)
- SIAFI (Empenho e Execução)
  - GERCOMP (Ordem de compromisso)
  - GEROP (Ordem de pagamento)
- Tesouro Gerencial (Extrator de dados do SIAFI)
- SCDP (Sistema de concessão de diárias e passagens)
- TransfereGov/SICONV (TEDs e convênios)
- ComprasNet (Licitações)

## Proposta de solução
- Gráfico de pizza representando total de orçamento(dotação) e quanto já foi empenhado, liquidado ou pago
  - orçamento total do órgão
  - por ação do qualitativo (SIOP)
- Gráfico de pizza mostrando pré-limite da LOA e quanto já foi planejado nas ações (referente ao quantitativo do siop)
- Total de financeiro e programação financeira
- Entradas e saídas de TEDs
- Quantitativo de TEDs não empenhadas pelos órgão executores
- Quantitativo de TEDs não empenhadas pelo IPEA