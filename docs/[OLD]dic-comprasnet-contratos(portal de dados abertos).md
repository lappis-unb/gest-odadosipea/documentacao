# Dados - Comprasnet/Contratos

## Dicionário de dados
### 1. Contratos
<!-- **API:** https://api.compras.dados.gov.br/  
**Endpoint:** comprasContratos/v1/contratos -->

**Base de dados abertos:** https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/comprasnet-contratos-anual-contratos-latest.csv
**Contratos dos anos anteriores:**
- https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2023/
- https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2022/
- https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2021/


| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 134845 | ID único do contrato. |
| numero | 00005/2022 | Número sequencial do contrato. |
| receita_despesa | Despesa | Indicativo de receita/despesa. |
| unidade_codigo | 113601 | Código da unidade organizacional responsável pela compra. |
| unidade_nome_resumido | IPEA/DF | Nome da unidade organizacional responsável pela compra. |
| codigo_tipo | 50 | Código do tipo de contrato. Outros tipos identificados são: 95 - Termo de compromisso, 94 - Carta contrato |
| tipo | Contrato | Descrição do tipo de contrato. |
| categoria | Serviços | Categoria do contrato. |
| objeto | PRESTAÇÃO, DE FORMA CONTÍNUA, DOS SERVIÇOS PÚBLICOS DE ABASTECIMENTO DE ÁGUA, ESGOTAMENTO SANITÁRIO ... | Descrição do objeto contratado. |
| data_publicacao | 2022-02-21 | Data de publicação da compra no Comprasnet |
| vigencia_inicio | 2022-02-14 | Início da vigência do contrato |
| vigencia_fim | null | Fim da vigência do contrato, quando o valor for `null`, indica que o contrato é por período indeterminado. |
| valor_inicial | 277115.76 | Valor anual do contrato. ?? |
| valor_global | 277115.76 | Valor anual do contrato. |
| num_parcelas | 1 | Quantidade de parcelas do contrato. |
| valor_parcela | 277115.76 | Valor da parcela do contrato. |
| valor_acumulado | 13855788 | Valor total do contrato. |
| fornecedor_cnpj_cpf_idgener | 00.082.024/0001-37 | CNPJ do fornecedor. |
| fornecedor_nome | COMPANHIA DE SANEAMENTO AMBIENTAL DO DISTRITO FEDERAL | Nome do fornecedor. |
| modalidade_codigo | 07 | Código da modalidade do contrato. |
| modalidade | Inexigibilidade | Descrição da modalidade do contrato. |

### 2. Faturas-Contratos

**Base de dados:** https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/comprasnet-contratos-anual-faturas-latest.csv

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 217388 | ID único da fatura. |
| contrato_id | 136823 | ID do contrato gerador da fatura. |
| emissao | 2023-10-16 | Data de emissão da fatura. |
| prazo | 2023-10-30 | Prazo para pagamento da fatura. |
| vencimento | 2023-10-30 | Prazo para vencimento da fatura. |
| valor | 18333.46 | Valor total da fatura. Valor líquido + Glosa + Multa + Juros |
| juros | 0.00 | Referente aos juros da fatura. |
| multa | 0.00 | Referente a multa por atraso da fatura. |
| glosa | 0.00 | Referente a glosa. |
| valor_liquido | 18333.46 | Referente ao valor líquido da fatura. |
| processo | 03001.001936/2021-79 | Número do processo no SEI. Utilizado pelo IPEA para identificar os pagamentos nos relatórios gerenciais. |
| situacao | Pago | Situação da fatura. |

### 3. Terceirizados-Contratos
<!-- **API:** https://api.compras.dados.gov.br/  
**Endpoint:** comprasContratos/doc/contrato/{id}/terceirizados   -->

**Base de dados abertos:** https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/comprasnet-contratos-anual-terceirizados-latest.csv
**Terceirizados dos anos anteriores:**
- https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2023/
- https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2022/
- https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2021/

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 33243 | ID único do terceirizado. |
| contrato_id | 134845 | ID do contrato responsável pelo terceirizado. |
| funcao | Eletricista | Função do terceirizado. |
| descricao_complementar | ELETRICISTA | Descrição complementar da função do terceirizado. |
| unidade | IPEA BRASÍLIA | Unidade organizacional onde o terceirizado está alocado. |
| custo | 4970.32 | Custo mensal do terceirizado. |
| escolaridade | 04 – Ensino Fundamental Completo | Grau de escolaridade do terceirizado. |
| data_inicio | 2022-02-02 | Início da vigência do contrato. |
| data_fim | 2022-08-10 | Fim da vigência do contrato. |
| situacao | Inativo | Situação do terceirizado. |

### 4. Parcelas-Contratos
| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 33243 | ID único da parcela. |
| contrato_id | 134845 | ID do contrato referente à parcela. |
| vigencia_inicio | 2022-02-14 | Início da vigência do contrato |
| vigencia_fim | null | Fim da vigência do contrato, quando o valor for `null`, indica que o contrato é por período indeterminado. |
| quantidade_parcelas | 12 | Quantidade de parcelas do contrato. |
| numero_parcela | 3 | Número da parcela em relação a quantidade total de parcelas. |
| valor_parcela | 277115.76 | Valor da parcela do contrato. |

## Tratamento dos dados  
1. Identificar valores pagos fora do desvio padrão e adicionar à tabela de inconsistências
   
2. Identificar divergências entre valor e valor liquido e calcular valor da fatura a partir do valor liquido, juros, mora, glosa.

| Variável | Variáveis | Descrição |
| --- | --- | --- |
| Valor pago | faturas/valor; faturas/valorliquido; faturas/juros; faturas/mora; faturas/glosa; | Define o valor pago da parcela a partir do valor total OU a partir dos valores discriminados. valor_pago=valor OU valor_pago=valorliquido+juros+mora+glosa |

3. Criar modelo de dados das parcelas dos contratos a partir da vigência início/fim, da quantidade de parcelas, e do valor das parcelas
