# Visão geral - Contratos

Neste dashboard serão apresentados os dados gerais dos contratos que estão sendo administrados pelo IPEA. Explorando a qualidade dos dados e insights quantitativos dos contratos.

### Público alvo  
- Servidores da coordenação de serviços gerais e apoio a pesquisa que lidam com a gestão de contratos 

### Objetivo  
- Apresentar quais contratos estão sendo administrados e a situação de cada um
- Apresentar a qualidade do dados de contratos gerados pelo ComprasGov para verificar se os processos internos de gestão de contratos estão corretos

### Escopo  
- Contratos administratos pelo IPEA disponíveis no ComprasGov

### Perguntas de negócio respondidas por esse dashboard  
- Os processos internos de gerenciamento dos contratos estão corretos
- Como o IPEA está contratando
- O que/quem o IPEA está contratando
- Quais contratações realizadas por cada unidade do IPEA (unidade de compra) 
- Quais contratações realizadas para cada unidade do IPEA (unidade requisitante) 
- Quantas contratações mensais o IPEA realiza
- Quais contratos estão próximos do fim da vigência

### Fonte dos dados:
1. ComprasGov.Contratos
2. Silver.Contratos_Empenhos_Anual
3. Silver.Contratos_Empenhos_Mensal

### Especificação

#### Indicadores
| Indicador | Observação | Tabela/Variáveis |
| --- | --- | --- |
| **Quantidade de contratos** |
| Total de contratos. | --- | ComprasGov.Contratos |
| Continuados. | Contratos com vigência maior ou igual à 2 anos e mais de uma parcela. | ComprasGov.Contratos |
| Pendentes de baixa. | Contratos com valor pago no tesouro gerencial igual ao valor contratual. | ComprasGov.Contratos, ComprasGov.Contratos.valor_global, Silver.Contratos_Empenhos_Anual.despesas_pagas_movim_liquido |
| Demais contratos. | --- | ComprasGov.Contratos |
| **Contratos** |
| Continuados. | --- | --- |
| Pendentes de baixa. | --- | --- |
| Demais contratos. | --- | --- |
| **Quantitativo de contratos** |
| Por categoria. | --- | ComprasGov.Contratos.categoria |
| Por modalidade. | --- | ComprasGov.Contratos.modalidade |
| Por unidade de compra. | --- | ComprasGov.Contratos.unidade_compra |
| Por unidade requisitante. | --- | ComprasGov.Contratos.unidades_requisitantes |
| Por tipo de contrato. | --- | ComprasGov.Contratos.tipo |
| Por tipo de fornecedor. | --- | ComprasGov.Contratos.fornecedor_tipo |
| Próximos do fim da vigência. | Contratos com término de vigência até 6 meses. | ComprasGov.Contratos, ComprasGov.Contratos.vigencia_fim |
| De contratações mensais | Quantas contratações mensais o IPEA realiza? | A DEFINIR |


