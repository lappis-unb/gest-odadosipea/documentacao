# Ecossistema de Software Livre para Gestão de Dados

O presente projeto objetiva o desenvolvimento científico e tecnológico de solução em software para subsidiar os processos de monitoramento e de tomada de decisão por meio da extração automatizada de dados e visualização de informações advindas de sistemas estruturantes empregados pela Diretoria de Desenvolvimento Institucional do Instituto de Pesquisa Econômica Aplicada (DIDES/IPEA).


Nesse espaço, serão compartilhadas toda a documentação técnica relacionada ao projeto, e os ponteiros para os códigos fontes e materiais desenvolvidos no projeto.

# Roadmap

Aqui vamos detalhar as macro etapas do projeto

## Fase de Onboarding

Checklist para a etapa de Onboarding do projeto

- Contratação da equipe do projeto
- Mapeamento de pessoas referências no IPEA/UnB 
- Mapeamento dos relatórios realizados manualmente  
- Mapeamento de fontes de dados 
- Mapeamento das tecnologias já utilizadas

## Sistemas Estruturantes

Locais com a lista de apis disponíveis - [https://www.gov.br/conecta/catalogo/apis/](https://www.gov.br/conecta/catalogo/apis/).

Todas as APIs tem um processo para ter acesso. Documentar o procedimento de como pedir acesso às APIs.

- SIAF (Sistema Integrado de Fincanças públicas) - acesso a API [AQUI](https://www.gov.br/conecta/catalogo/apis/siafi-2013-modulo-orcamentario)

- SIAPE (Sistema Integrado de Administração de Pessoal) - Acesso API [AQUI](https://www.gov.br/conecta/catalogo/apis/consulta-siape)