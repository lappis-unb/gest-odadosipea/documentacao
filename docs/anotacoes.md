# Anotações

## Sugestões melhoria IPEA
- Incluir informações mais detalhadas nas faturas, como por exemplo nas faturas de energia geral/relogio bomba de incêndio, para permitir separação e filtragem nos gráficos


## Dúvidas IPEA
- Como os valores do FNSP são vinculados ao IPEA?
- Os valores disponíveis no PNCP, do plano de compras anual, conta com os valores dos contratos recorrentes realizados em outros anos?

## Tarefas para mim
- Reuniões negociais para terminar a construção dos relatórios
- Reuniões negociais para validar os dados gerados pelos relatórios
- Finalizar especificação dos dash de inconsistências dos contratos
- Finalizar especificação dos dash dos contratos específicos
- Analisar dados do SIOP para identificar necessidades de tratamento dos dados
- Bater valores dos dashs com o do PNCP
- Iniciar análise dos dados do SIAFI ou SIAPE

## Tarefas time de dados
- Criar modelo de dados de parcelas dos contratos e bater com as faturas realizadas no mesmo período
- Tratar valores das datas
- Calcular valor planejado x gasto no ano
- Fazer DAG de extração dos dados do SIOP para o MINIO
- Fazer DAG de ingestão dos dados do SIOP no MINIO para o POSTGRES
- Fazer DAG de transformação dos dados do SIOP usando DBT

## Geral
- Fazer seleção para o time de dados
    - 2 devs dados jrs
    - Ver com Joyce/Carla sobre quem vai ser
- Ver como vai ser o repasse para os novos membros, visto que a Joyce não está
