# Relatório individual do contrato

Relatório de acompanhamento individual dos contratos.

### 1. Objetivo:  
- Fornecer um panorama do planejamento/execução orçamentária de cada contrato
- Fornecer saldo total e saldo disponível da execução orçamentária de cada contrato
- Fornecer dados estatísticos sobre o contrato

### 2. Público alvo:  
- Presidência/Diretoria
- Público externo
- Coordenação de serviços gerais e apoio a pesquisa

### 3. Escopo:  
- Orçamento planejado total de cada contrato vigente do IPEA

### 4. Principais perguntas de negócio:  
- Qual o limite para gastos excedentes?
- Quanto posso cortar do orçamento de cada contrato para alocar para outros gastos? (contingenciamento)
- Como posso melhorar o planejamento dos contratos?

## Fonte dos dados:
1. https://api.compras.dados.gov.br/
2. https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/comprasnet-contratos-anual-contratos-latest.csv
3. https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/comprasnet-contratos-anual-faturas-latest.csv
4. https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/comprasnet-contratos-anual-terceirizados-latest.csv
5. Contratos, faturas e terceirizados dos anos anteriores:
    - https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2023/
    - https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2022/
    - https://repositorio.dados.gov.br/seges/comprasnet_contratos/anual/2021/



## Especificação
### Dados do contrato
| Variável | Descrição/Hint | Variáveis |
| --- | --- | --- |
| Objeto contratato | Descrição do objeto contratado. | contrato/objeto |
| Unidade | Nome da unidade organizacional responsável pela compra. | contrato/unidade_nome_resumido |
| Fornecedor | Nome do fornecedor. | contrato/fornecedor_nome |
| Categoria | Categoria do contrato. | contrato/categoria |
| Vigência | Vigência do contrato. | contrato/vigencia_inicio; contrato/vigencia_fim |

### Indicadores
| Indicador | Descrição/Hint | Variáveis | Método de cálculo/classificação |
| --- | --- | --- | --- |
| **Execução orçamentária geral** |
| Orçamento total previsto | Orçamento total previsto para despesas do contrato. | contrato/valor_parcela | Somatório do valor do contrato no período. |
| Orçamento executado | Orçamento executado do contrato. | faturas/valor; faturas/situacao | Somatório do valor das faturas com situação **paga**. |
| Orçamento a executar | Orçamento do contrato com previsão de execução. | contrato/valor_parcela; faturas/valor; faturas/situacao | Somatório do valor das parcelas dos meses não executados e das faturas com situação **pendente**. |
| Saldo disponível | Saldo disponível para realocação. | indicadores/orçamento_previsto; indicadores/orçamento_executado; indicadores/orçamento_a_executar | Saldo disponível = orçamento total previsto - orçamento executado - orçamento a executar |
| **Execução orçamentária mensal** |
| Orçamento total previsto por mês | Orçamento mensal previsto no contrato. | parcelas/valor_parcela | --- |
| Orçamento executado por mês | Orçamento do contrato executado por mês. | faturas/valor | --- |

### Tabela de listagem de parcelas/faturas
| Variável | Descrição/Hint | Variáveis |
| --- | --- | --- |
| Mês/Ano | Descrição do objeto contratado. | parcela/data |
| Número da parcela | Número da parcela/Quantidade total de parcelas | parcelas/numero_parcela; parcelas/quantidade_parcelas |
| Valor planejado | Valor planejado. | parcela/valor_parcela |
| Valor pago | Valor pago. | fatura/valor |
| Situação | Situação da fatura. | fatura/situacao |

### Filtros  
- Período

### Protótipo
[Protótipo - Relatório individual do contrato](https://www.figma.com/proto/tSADvo07tQVCt993BuMmMk/Esbo%C3%A7o---Dashboards?page-id=80%3A161&node-id=80-162&t=R1SrGKqtHDzIyUgt-0&scaling=min-zoom&content-scaling=fixed)
