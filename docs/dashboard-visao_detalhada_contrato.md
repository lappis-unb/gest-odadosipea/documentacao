# Visão geral - Contratos

Neste dashboard serão apresentados os dados detalhados de um determinado contrato formado no IPEA. Explorando execução orçamentária e financeira do contrato.

### Público alvo  
- Servidores da coordenação de serviços gerais e apoio a pesquisa que lidam com a gestão de contratos 
  
### Objetivo  
- Fornecer um panorama do planejamento/execução orçamentária de cada contrato
- Fornecer uma visão histórica da execução orçamentária/financeira do contrato
- Fornecer saldo total e saldo disponível da execução orçamentária de cada contrato

### Escopo  
- Contratos administratos pelo IPEA disponíveis no ComprasGov
- Execução orçamentária/financeira dos contratos

### Perguntas de negócio respondidas por esse dashboard  
- Qual o limite para gastos excedentes
- Quanto posso cortar do orçamento de cada contrato para alocar para outros gastos (contingenciamento)
- Como posso melhorar o planejamento dos contratos

## Fonte dos dados:
1. ComprasGov.Contratos
2. ComprasGov.Faturas 
3. ComprasGov.Cronograma
4. ComprasGov.Empenhos
5. Silver.Contratos_Empenhos_Anual
6. Silver.Contratos_Empenhos_Mensal

## Especificação
### Dados do contrato
| Variável | Descrição/Hint | Variáveis |
| --- | --- | --- |
| Objeto contratato | Descrição do objeto contratado. | ComprasGov.Contratos.objeto |
| Unidade | Nome da unidade organizacional responsável pela compra. | ComprasGov.Contratos.unidade_nome_resumido |
| Fornecedor | Nome do fornecedor. | ComprasGov.Contratos.fornecedor_nome |
| Categoria | Categoria do contrato. | ComprasGov.Contratos.categoria |
| Vigência | Vigência do contrato. | ComprasGov.Contratos.vigencia_inicio; ComprasGov.Contratos.vigencia_fim |

### Dados orçamentários contratuais
| Variável | Descrição/Hint | Variáveis |
| --- | --- | --- |
| Valor contratado. | Valor contratado (anual ou parcela única). | ComprasGov.Contratos.valor_global |
| Quantidade de parcelas. | Quantidade de parcelas (anuais) | ComprasGov.Contratos.num_parcelas |
| Valor da parcela. | Valor da parcela. | ComprasGov.Contratos.valor_parcela |
| Valor acumulado. | Valor acumulado até o momento. | ComprasGov.Contratos.valor_acumulado |

### Indicadores orçamentários
| Indicador | Descrição/Hint | Variáveis | Método de cálculo/classificação |
| --- | --- | --- | --- |
| **Orçamento contratual** |
| Valor contratual previsto (cronograma) | Orçamento contratual previsto para despesas do contrato no período. | ComprasGov.Cronograma.valor | Somatório do valor previsto para o contrato no período. |
| Orçamento executado (faturas) | Orçamento declarado como executado do contrato. | ComprasGov.Faturas.valor; ComprasGov.Faturas.situacao | Somatório do valor das faturas com situação **pago**. |
| Orçamento a liquidar (faturas) | Orçamento a liquidar do contrato. | ComprasGov.Faturas.valor; ComprasGov.Faturas.situacao | Somatório do valor das faturas com situação **pendente**. |
| Orçamento a executar | Orçamento do contrato com previsão de execução no período. | ComprasGov.Cronograma.valor; ComprasGov.Cronograma.vencimento | Somatório do valor previsto e não executado para o contrato no período. |
| Saldo contratual disponível. | Saldo contratual disponível. | ?? | Saldo disponível = Valor contratual previsto (cronograma) - orçamento executado (faturas) - orçamento a executar (cronograma) |
| **Execução orçamentária (Siafi)** |
| Valor empenhado | Valor empenhado para o contrato no período. | Silver.Contratos_Empenhos_Anual.despesas_empenhadas_movim_liquido | Somatório do valor empenhado para o contrato no período. |
| Valor liquidado | Valor liquidado para o contrato no período. | Silver.Contratos_Empenhos_Anual.despesas_liquidadas_movim_liquido | Somatório do valor liquidado para o contrato no período. |
| Valor pago | Valor pago para o contrato no período. | Silver.Contratos_Empenhos_Anual.despesas_pagas_movim_liquido | Somatório do valor pago para o contrato no período. |

### Histórico de execução orçamentária
| Indicador | Descrição/Hint | Variáveis | Método de cálculo/classificação |
| --- | --- | --- | --- |
| **Execução orçamentária mensal** |
| Orçamento contratual previsto por mês (cronograma) | --- | ComprasGov.Cronograma.valor | --- |
| Orçamento executado por mês (faturas) | Orçamento do contrato declarado como executado por mês. | ComprasGov.Faturas.valor; ComprasGov.Faturas.situacao; ComprasGov.Faturas.ateste (extrair mês e ano) | Faturas com situação **pago**. |
| Orçamento a executar (faturas) | Orçamento do contrato com previsão de execução. | ComprasGov.Faturas.valor; ComprasGov.Faturas.situacao | Faturas com situação **pendente**. |
| Orçamento empenhado por mês. | Valor empenhado no Siafi. | Silver.Contratos_Empenhos_Mensal.despesas_empenhadas_movim_liquido | --- |
| Orçamento liquidado por mês. | Valor liquidado no Siafi. | Silver.Contratos_Empenhos_Mensal.despesas_liquidadas_movim_liquido | --- |
| Orçamento pago por mês. | Valor pago no Siafi. | Silver.Contratos_Empenhos_Mensal.despesas_pagas_movim_liquido | --- |

### Detalhamento
| Variável | Descrição/Hint | Variáveis |
| --- | --- | --- |
| **Tabela de listagem - Valores empenhados** |
| Mês | Mês de referência do empenho. | Silver.Contratos_Empenhos_Mensal.ne_ccor_mes_emissao |
| Ano | Ano de referência do empenho. | Silver.Contratos_Empenhos_Mensal.ne_ccor_ano_emissao |
| Valor | Valor do empenho. | Silver.Contratos_Empenhos_Mensal.despesas_empenhadas_movim_liquido |
| **Tabela de listagem - Valores liquidados** |
| Mês | Mês de referência da liquidação. | Silver.Contratos_Empenhos_Mensal.ne_ccor_mes_emissao |
| Ano | Ano de referência da liquidação. | Silver.Contratos_Empenhos_Mensal.ne_ccor_ano_emissao |
| Valor | Valor da liquidação. | Silver.Contratos_Empenhos_Mensal.despesas_liquidadas_movim_liquido |
| **Tabela de listagem - Valores pagos** |
| Mês | Mês de referência do pagamento. | Silver.Contratos_Empenhos_Mensal.ne_ccor_mes_emissao |
| Ano | Ano de referência do pagamento. | Silver.Contratos_Empenhos_Mensal.ne_ccor_ano_emissao |
| Valor | Valor do pagamento. | Silver.Contratos_Empenhos_Mensal.despesas_pagas_movim_liquido |
| **Tabela de listagem - Faturas** |
| Mês | Mês de referência da fatura. | ComprasGov.Faturas.ateste (extrair mês) |
| Ano | Ano de referência da fatura. | ComprasGov.Faturas.ateste (extrair ano) |
| Valor | Valor da fatura. | ComprasGov.Faturas.valor |
