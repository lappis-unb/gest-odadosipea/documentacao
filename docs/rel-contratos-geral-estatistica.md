# Relatório de faturas com inconsistencias

Relatório de estatisticas gerais dos estatística dos contratos

### 1. Objetivo:  
- Fornecer uma visão critica dos dados

### 2. Público alvo:  
- Presidência/Diretoria
- Público externo
- Coordenação de serviços gerais e apoio a pesquisa

### 3. Escopo:  
- Todos os contratos do IPEA

### 4. Principais perguntas de negócio:  
- Quantos e quais os contratos vigentes do IPEA

## Fonte dos dados:
1. Contratos

## Especificação

### Indicadores
| Indicador | Descrição/Hint | Variáveis | Método de cálculo/classificação |
| --- | --- | --- | --- |
| Quantidade total de contratos | --- | --- | --- |
| Quantidade de contratos por categoria | --- | --- | --- |

