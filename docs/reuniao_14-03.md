# Reunião IPEA 14/03
Reunião realizada presencialmente no IPEA junto ao diretor da DIDES, Fernando.  

## Principais problemas identificados
1. A necessidade do IPEA é um portal de transparência com níveis de acesso: interno e externo.
2. Orçamento:
   1. Visão completa sobre o orçamento:
      1. Servidores
      2. Bolsas
      3. Diárias/Passagens
      4. Como o IPEA está gastando?
   2. Efetividade das contratações a partir da porcentagem em relação aos gastos totais
   3. Estimativas de compras recorrentes
   4. Programação do curto e médio prazo
3. Pessoal:
   1. Dados mais confiáveis
   2. Dados mais transparentes
   3. Dados mais manipuláveis
   4. Dados de remoção/realocação
   5. Dados de escolaridade
   6. Crítica aos dados
   7. Histórico de competencia e formação dos servidores
4. Contratos:
   1. Necessidades:
      1. Contratos recorentes 
      2. Novos contratos
      3. Onde cada contrato está no processo
      4. Melhorar a visualização de contratos com pagamento único
   2. Problemas:
      1. Mostrar que o IPEA não está planejando direito
      2. Dificuldade de ver o que pode ser cortado
      3. Não tem visão onde estão as demandas e quais os retornos