# Análise dos relatórios

## Coordenação de apoio a pesquisa
A coordenação de apoio a pesquisa realiza 3 atividades:
- contrata e gerencia os bolsistas
- celebra instrumentos de parceria com universidades
- faz análise de prestação de contas de instrumentos de parceria

Os relatórios enviados são basicamente do SisBolsas, sistema não estruturante utilizado para controle de bolsas.

**Dúvidas**
- A celebração dos instrumentos de parceria com as universidades é realizada por TEDs ou por Acordo do cooperação técnica ACTs?
  - Se for realizada por TED, qual procedimento vocês fazem em relação ao financeiro?
- Os dados de pagamento de bolsas vão para o siafi de que forma?


## Coordenação de serviços gerais
A coordenação de serviços gerais faz o gerenciamento dos contratos de manutenção predial, água e anergia e patrimonio


Os relatórios enviados envolvem:
**Forneciemnto de água**
1. Controle financeiro
- Nº Contrato
- % de rateio com IPHAN
- Processo no SEI
- Orçamento previsto
  - Total
  - Por mês
- Orçamento realizado
  - Por órgão
  - Total
  - Por mês
- Saldo restante
  - Total
  - Por mês

2. Acompanhamento do contrato mensal
- Data leitura anterior/mês
- Data leitura atual/mês
- Consumo faturado(m³)/mês
- Data Próxima leitura
- Valor

**Fornecimento de energia**
1. Controle financeiro
- Nº Contrato
- % de rateio com IPHAN
- Processo no SEI
- Orçamento previsto
  - Total
  - Por mês
- Orçamento realizado
  - Por órgão
  - Total
    - Relógio geral
    - Relógio bomba de incêndio
  - Por mês
- Saldo restante
  - Total
  - Por mês

2. Acompanhamento do contrato mensal
- Mês referência (conta-mês)
- Vencimento
- Consumo faturado(kwh)/mês
- Data leitura anterior/mês
- Data leitura atual/mês
- Data Próxima leitura
- Valor bruto/mês
- Valor impostos/mês
- ICMS/mês
- Total a pagar

**Manutenção predial**
-

**Dúvidas**
- Os contratos de água e energia são assinados pelo IPEA?
- Como ocorre o rateio com o IPHAN? É por meio de TEDs?
- O controle desse orçamento no SIAFI e SIOP é realizado por vocês ou pela CGPGO?
  - Se for realizado por eles, como é feita essa comunicação?
- Porque o relógio da bomba de incêndio é separado? é um outro contrato?


**Anotações**
**contrataç~eos e contratos**
PNCP - portal nacional de compras publicas
plano anual de contratações
realizado antes da loa, depois, quando a loa é aprovada, o sistema reabre pra fazer a adequação aos valores da loa

SIOP - especifico de pessoal
pncp - especifico de compras, licitações

! acompanhamento mês a mês da estimativa de contratos
! acompanhamento anual de contratos 
! estimativa x executado

pncp faz o orçamento com valores estimados, a execução pode ser maior ou menor
cruzamento entre pncp e contratos para saber quanto foi executado e quanto resta

contratos tem o valor da parcela contratada e da parcela executada em locais diferentes

alertas sobre limites
contigenciamento de recursos

plano de contrataão anual vem das diretorias e é agregado pela coordenação de compras e contratos no pncp

! bater os valores contratados x executados para melhorar o planejamento


**controle de patrimonio**
SIADS
contratos usando o comprasnet
não tem relatórios gereciais
- quais artefatos não estao preenchidos

tercerização, limpeza

contrato de manutenção orbita em tercerização e insumos

contratos de ordem de serviço
o contrato geral fica no comprasnet, mas as ordems de serviço não
informações sobre oq a casa tem demandado fica no sistema éPedido
SEI e PLANILHAS para gerenciar ordem de serviços
ordem de serviços chega pelo sei, fazem a conferencia

epedidos é a necessidade do usuário
oq ele precisa, 

Acordo com IPHAN é feito por TED? como é controlado o

Almoxarifado virtual, de uma empresa terceira
os itens solicitados não entram na contabilidade
entra como serviço e não como bem

Quantidade contratada fica no comprasnet?
Não, só na nota fiscal

Sistema da empresa pode ter os itens solicitados

Contratos compartilhados
- lançamento já é feito dividido
- estimativa é feita total mas a execução é feita dividida

estimativa do orçamento repassado pelo iphan?

## Coordenação de licitações e contratos


## Coordenação de apoio a pesquisa
Processo de bolsas
TEDs e ACTs

Quantos TEDs, Quantos ACTs

**Processo de bolsas**
! quantidade de bolsistas por área
! previsaão de chamamento
! orçamento por área
! orçamento por instituição

sisbolsas
- processo de chamamento publico/edital
  - critérios de seleção
  - perfil do bolsista
  - comissão de julgamento
  - inscrição dentro do sistema
  - disponibiliza para a comissão de julgamento a relação de bolsistas
  - curriculos, entrevistas de acordo com o processo seletivo
  - resultado dentro do sistema
  - prazo de recurso
  - resultado final dentro do sistema
- Administração de bolsas
  - celebração do termo de contratação do bolsista
  - rotina de acompanhamento pela área tecnica
  - geração da folha de pagamento pelo sistema
  - enviado para unidade orçamentária fazer o pagamento
  - declaração de disponibilidade orçamentária solicitada para o wagner

relatórios gerados pelo super7 ?

Dotação com o wagner
empenho realizado pelo wagner
! podem ter bolsas suspensas e os valores ficam parados

além da bolsa tem o auxilio financeiro
prestação de contas no sisbolsas

cartão bolsista
cartão recebe um orçamento x e vai executando

Sisbolsas tem API
não tem como gerar relatórios, só por extração de dados via excel

renovações são filtradas na mão

incorporar 

disponibilidade orçamentária via SEI e SIAFI ou SIOP
ela bloqueia o orçamento como se fosse um pré-empenho

! saber quanto já foi reservado e quanto já foi executado

podem ter diversos recursos, como TEDs
saber oq executou por fonte
previsto no SEI e executado no SIAFI

! acompanhamento do plano de trabalho no TED
isso tem no transferegov

## coordenação de apoio a pesquisa parte 2

sistema colabore, sistema colaborar
SGAC - Sistema de Gestão de Acordos de Cooperação Técnica, Convênios e Contratos
relatórios com duplicidade de registros

Colaborar é um sharepoint

campo tipo de instrumento está sendo apagado

Acordo de cooperação Tecnica
Termo de execução descentralizada
Convênios (sem ser convênio do siconv)
Contratos
Termo de colaboração

Só a TED vai pros sistemas estruturantes
Termo de fomento e termo de colaboração tem transferencia de recursos, como é feita??
PROEV - pró evento 
- norma para apoio a eventos de disceminação de conhecimento
- chamada para os eventos é feito no sisbolsas

Termo de fomento e de colaboração é feito com organizações sociais
TEDs com órgãos
Convenios com municipios
PROEV é pago com cartão de auxilio a pesquisa

! com quais instituições o ipea tem acordo
- referente a todos os tipos de acordo, não só as TEDS
- demonstram quais áreas estão atuando
- quais produtos vão ser gerados
  

**Acesso as APIs**
- cancelar registro de certificado a1 pelo lappis

- Solicitar certificado a1
- Solicitar acesso ao SIAPE pois não precisa do certificado
- Solicitar usuário do ComprasNet
- Criar VPN, acessos
- 




