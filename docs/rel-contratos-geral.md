# Relatório geral de contratos

Relatório de acompanhamento geral dos contratos.

### 1. Objetivo:  
- Fornecer um panorama geral do planejamento/execução orçamentária de todos os contratos
- Fornecer saldo total e saldo disponível da execução orçamentária dos contratos
- Fornecer dados estatísticos sobre os contratos

### 2. Público alvo:  
- Presidência/Diretoria
- Público externo
- Coordenação de serviços gerais e apoio a pesquisa

### 3. Escopo:  
- Orçamento planejado total de todos os contratos vigentes do IPEA

### 4. Principais perguntas de negócio:  
- Qual o limite para gastos excedentes?
- Quanto posso cortar do orçamento dos contratos para alocar para outros gastos? (contingenciamento)
- Como posso melhorar o planejamento dos contratos?

## Fonte dos dados:
1. Contratos
2. Faturas
3. Terceirizados
4. Parcelas

## Especificação

### Indicadores
| Indicador | Descrição/Hint | Variáveis | Método de cálculo/classificação |
| --- | --- | --- | --- |
| **Execução orçamentária geral** |
| Orçamento total previsto | Orçamento total previsto para despesas com contratos. | contrato/valor_parcela | Somatório do valor do contrato no período. |
| Orçamento executado | Orçamento executado pelo órgão com contratos. | faturas/valor; faturas/situacao | Somatório do valor das faturas com situação **paga**. |
| Orçamento a executar | Orçamento com previsão de execução pelo órgão com contratos. | contrato/valor_parcela; faturas/valor; faturas/situacao | Somatório do valor das parcelas dos meses não executados e das faturas com situação **pendente**. |
| Saldo disponível | Saldo disponível para realocação. | indicadores/orçamento_previsto; indicadores/orçamento_executado; indicadores/orçamento_a_executar | Saldo disponível = orçamento total previsto - orçamento executado - orçamento a executar |
| **Execução orçamentária por categoria** |
| Orçamento total previsto por categoria | Orçamento total previsto pelo órgão para despesas com contratos de uma determinada categoria. | As mesmas do orçamento total previsto; contrato/categoria | Orçamento previsto/Categoria |
| Orçamento executado por categoria | Orçamento executado pelo órgão com contratos de uma determinada categoria. | As mesmas do orçamento executado; contrato/categoria | Orçamento executado/categoria |
| Orçamento a executar por categoria | Orçamento de contratos de uma determinada categoria com previsão de execução. | As mesmas do orçamento a executar; contrato/categoria | Orçamento a executar/categoria |
| Saldo disponível por categoria | Saldo disponível para realocação por categoria. | As mesmas do saldo disponível; contrato/categoria | Saldo disponível/categoria |
| **Execução orçamentária mensal** |
| Orçamento total previsto por mês | Orçamento mensal previsto pelo órgão para despesas com contratos. | --- | --- |
| Orçamento executado por mês | Orçamento executado pelo órgão com contratos por mês. | --- | --- |

#### Tabela de listagem dos contratos
| Variável | Descrição/Hint | Variáveis |
| --- | --- | --- |
| Objeto | Descrição do objeto contratado. | contrato/objeto |
| Valor total | Valor total do contrato. | contrato/valor_acumulado |
| Valor anual | Valor anual do contrato. | contrato/valor_global |
| Vigência | Período de vigência do contrato. | contrato/vigencia_inicio; contrato/vigencia_fim |
| Parcelas | Quantidade de parcelas do contrato. | contrato/num_parcelas |
  
### Filtros  
- Período

### Protótipo
[Protótipo - Relatório geral de contratos](https://www.figma.com/proto/tSADvo07tQVCt993BuMmMk/Esbo%C3%A7o---Dashboards?page-id=26%3A530&node-id=56-162&t=R1SrGKqtHDzIyUgt-0&scaling=min-zoom&content-scaling=fixed)

