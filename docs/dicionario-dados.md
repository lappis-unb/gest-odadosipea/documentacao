# Dicionário de dados

## ComprasGov
**Forma de extração:** API `https://contratos.comprasnet.gov.br/api/`

### Contratos
**Endpoint:** `/contrato/ug/{unidade-codigo}`
Retorna todos os contratos ativos de uma determinada UG (unidade gestora).

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 2443 | ID único do contrato. |
| numero | 00006/2019 | Número sequencial do contrato. |
| receita_despesa | Despesa | Indicativo de receita/despesa. |
| contratante.orgao_origem.codigo | 61201 | Código do órgão de origem. |
| contratante.orgao_origem.nome | INSTITUTO DE PESQUISA ECONOMICA APLICADA | Nome do órgão de origem. |
| contratante.orgao_origem.unidade_gestora_origem.codigo | 113601 | Código da unidade gestora de origem. |
| contratante.orgao_origem.unidade_gestora_origem.nome_resumido | IPEA/DF | Nome resumido da unidade gestora de origem. |
| contratante.orgao_origem.unidade_gestora_origem.sisg | Sim | Indicativo de uso do SISG pela unidade gestora. |
| contratante.orgao_origem.unidade_gestora_origem.utiliza_siafi | Sim | Indicativo de uso do SIAFI pela unidade gestora. |
| contratante.orgao_origem.unidade_gestora_origem.utiliza_antecipagov | Sim | Indicativo de uso do AntecipaGov pela unidade gestora. |
| contratante.orgao.codigo | 61201 | Código do órgão contratante. |
| contratante.orgao.nome | INSTITUTO DE PESQUISA ECONOMICA APLICADA | Nome do órgão contratante. |
| contratante.orgao.unidade_gestora.codigo | 113601 | Código da unidade gestora contratante. |
| contratante.orgao.unidade_gestora.nome_resumido | IPEA/DF | Nome resumido da unidade gestora contratante. |
| fornecedor.tipo | JURIDICA | Tipo do fornecedor (Física ou Jurídica). |
| fornecedor.cnpj_cpf_idgener | 00.000.000/0001-91 | Identificação do fornecedor (CNPJ/CPF). |
| fornecedor.nome | BANCO DO BRASIL SA | Nome do fornecedor. |
| codigo_tipo | 50 | Código do tipo de contrato. |
| tipo | Contrato | Descrição do tipo de contrato. |
| subtipo | null | Subtipo do contrato (se aplicável). |
| prorrogavel | null | Indicativo se o contrato é prorrogável. |
| situacao | Ativo | Situação atual do contrato. |
| justificativa_inativo | null | Justificativa para inativação do contrato (se aplicável). |
| categoria | Serviços | Categoria do contrato. |
| subcategoria | null | Subcategoria do contrato (se aplicável). |
| unidades_requisitantes | DIDES/CGCAP | Unidades requisitantes do contrato. |
| processo | 03001.004484/2018-81 | Número do processo relacionado ao contrato. |
| objeto | PRESTAÇÃO DE SERVIÇOS RELATIVOS À EMISSÃO E ADMINISTRAÇÃO DE CARTÃO COM FUNÇÃO DE CRÉDITO DENOMINADO CARTÃO BB PESQUISA. | Descrição do objeto contratado. |
| amparo_legal | | Base legal do contrato. |
| informacao_complementar | null | Informações complementares do contrato. |
| codigo_modalidade | 07 | Código da modalidade do contrato. |
| modalidade | Inexigibilidade | Descrição da modalidade do contrato. |
| unidade_compra | 113601 | Código da unidade responsável pela compra. |
| licitacao_numero | 00002/2019 | Número da licitação vinculada ao contrato. |
| sistema_origem_licitacao | null | Sistema de origem da licitação (se aplicável). |
| data_assinatura | 2019-01-31 | Data de assinatura do contrato. |
| data_publicacao | 2019-02-04 | Data de publicação da compra no Comprasnet. |
| data_proposta_comercial | null | Data da proposta comercial (se aplicável). |
| vigencia_inicio | 2019-02-04 | Início da vigência do contrato. |
| vigencia_fim | 2025-02-04 | Fim da vigência do contrato. |
| valor_inicial | 0,01 | Valor inicial do contrato. |
| valor_global | 0,01 | Valor global do contrato. |
| num_parcelas | 1 | Quantidade de parcelas do contrato. |
| valor_parcela | 0,01 | Valor da parcela do contrato. |
| valor_acumulado | 0,61 | Valor acumulado do contrato. |
| links.historico | https://contratos.comprasnet.gov.br/api/contrato/2443/historico | Link para o histórico do contrato. |
| links.empenhos | https://contratos.comprasnet.gov.br/api/contrato/2443/empenhos | Link para os empenhos do contrato. |
| links.cronograma | https://contratos.comprasnet.gov.br/api/contrato/2443/cronograma | Link para o cronograma do contrato. |
| links.garantias | https://contratos.comprasnet.gov.br/api/contrato/2443/garantias | Link para as garantias do contrato. |
| links.itens | https://contratos.comprasnet.gov.br/api/contrato/2443/itens | Link para os itens do contrato. |
| links.prepostos | https://contratos.comprasnet.gov.br/api/contrato/2443/prepostos | Link para os prepostos do contrato. |
| links.responsaveis | https://contratos.comprasnet.gov.br/api/contrato/2443/responsaveis | Link para os responsáveis pelo contrato. |
| links.despesas_acessorias | https://contratos.comprasnet.gov.br/api/contrato/2443/despesas_acessorias | Link para as despesas acessórias do contrato. |
| links.faturas | https://contratos.comprasnet.gov.br/api/contrato/2443/faturas | Link para as faturas do contrato. |
| links.ocorrencias | https://contratos.comprasnet.gov.br/api/contrato/2443/ocorrencias | Link para as ocorrências do contrato. |
| links.terceirizados | https://contratos.comprasnet.gov.br/api/contrato/2443/terceirizados | Link para os terceirizados do contrato. |
| links.arquivos | https://contratos.comprasnet.gov.br/api/contrato/2443/arquivos | Link para os arquivos relacionados ao contrato. |


### Faturas
**Endpoint:** `/contrato/{contrato-id}/faturas`
Retorna uma lista com todas as faturas dos contratos (valores a serem pagos).

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 454895 | ID único da fatura. |
| contrato_id | 140731 | ID do contrato gerador da fatura. |
| tipolistafatura_id | PRESTAÇÃO DE SERVIÇOS | Tipo de lista da fatura. |
| justificativafatura_id | Ordem Lista: Seguindo a ordem cronológica da lista. | Justificativa para a emissão da fatura. |
| sfadrao_id |  | Identificador padrão SF (se aplicável). |
| numero | 7501558 | Número da fatura. |
| emissao | 2024-12-02 | Data de emissão da fatura. |
| prazo | 2024-12-17 | Prazo para pagamento da fatura. |
| vencimento | 2024-12-10 | Prazo para vencimento da fatura. |
| valor | 3.324,47 | Valor total da fatura. Valor líquido + Glosa + Multa + Juros. |
| juros | 0,00 | Referente aos juros da fatura. |
| multa | 0,00 | Referente à multa por atraso da fatura. |
| glosa | 0,00 | Referente à glosa da fatura. |
| valorliquido | 3.324,47 | Valor líquido da fatura. |
| processo | 03001.004108/2024-35 | Número do processo no SEI. Utilizado pelo IPEA para identificar os pagamentos nos relatórios gerenciais. |
| protocolo | 2024-12-03 | Data do protocolo da fatura. |
| ateste | 2024-12-03 | Data do ateste da fatura. |
| repactuacao | Não | Indica se há repactuação na fatura. |
| infcomplementar | - | Informações complementares da fatura. |
| mesref | 11 | Mês de referência da fatura (se aplicável). |
| anoref | 2024 | Ano de referência da fatura (se aplicável). |
| situacao | Pago | Situação atual da fatura. |
| chave_nfe | - | Chave da Nota Fiscal Eletrônica vinculada (se aplicável). |
| dados_empenho.id_empenho | 12394513 | Identificador único do empenho associado. |
| dados_empenho.numero_empenho | 2024NE000312 | Número do empenho associado à fatura. |
| dados_empenho.valor_empenho | 3.324,47 | Valor do empenho associado. |
| dados_empenho.subelemento | 43 | Sub-elemento relacionado ao empenho. |
| dados_referencia.mesref | 11 | Mês de referência do pagamento. |
| dados_referencia.anoref | 2024 | Ano de referência do pagamento. |
| dados_referencia.valorref | 3.324,47 | Valor referente ao pagamento no período informado. |
| dados_item_faturado.id_item_contrato | 1197455 | Identificador único do item do contrato faturado. |
| dados_item_faturado.quantidade_faturado | 1,00000 | Quantidade faturada do item do contrato. |
| dados_item_faturado.valorunitario_faturado | 3.324,4700 | Valor unitário faturado do item do contrato. |
| dados_item_faturado.valortotal_faturado | 3.324,47 | Valor total faturado do item do contrato. |

### Cronograma
**Endpoint:** `/contrato/{contrato-id}/cronograma`
Retorna uma lista com o cronograma de pagamento do contrato (valores planejados).

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 217388 | ID único da fatura. |
| contrato_id | 136823 | ID do contrato gerador da fatura. |
| tipo | Contrato | Descrição do tipo de contrato. |
| numero | 00007/2022 | Número sequencial do contrato. |
| receita_despesa | Despesa | Indicativo de receita/despesa do contrato. |
| observacao | CELEBRAÇÃO DO CONTRATO: 00007/2022 DE ACORDO COM PROCESSO NÚMERO: 03001.001940/2021-37 | Observação do item do cronograma, utilizado para indicar repactuação, mudanças nos valores e pagamentos retroativos. |
| mesref | 11 | Mês de referência do pagamento. |
| anoref | 2024 | Ano de referência do pagamento. |
| vencimento | 2024-12-10 | Prazo para vencimento do pagamento. |
| retroativo | Sim | Indicador se o item do cronograma de pagamentos é retroativo ou não. |
| valor | 3.324,47 | Valor planejado para o mês/ano de referência. |


### Empenhos
**Endpoint:** `/contrato/{contrato-id}/empenhos`
Retorna uma lista com todos os empenhos do contrato.

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 10165969 | Identificador único do empenho. |
| unidade_gestora | 113601 | Código da unidade gestora responsável. |
| gestao | 11302 | Código de gestão associado ao empenho. |
| numero | 2023NE000139 | Número do empenho. |
| data_emissao | 2023-02-22 | Data de emissão do empenho. |
| credor | 07.522.669/0001-92 - NEOENERGIA DISTRIBUICAO BRASILIA S.A. | Identificação do credor, incluindo CNPJ e nome. |
| fonte_recurso | 1000000000 | Fonte de recurso utilizada no empenho. |
| programa_trabalho | null | Programa de trabalho vinculado (se aplicável). |
| planointerno | 100ADMNIBSB - ADMINISTRACAO DA UNIDADE | Código e descrição do plano interno. |
| naturezadespesa | 339039 - OUTROS SERVICOS DE TERCEIROS - PESSOA JURIDICA | Natureza da despesa associada ao empenho. |
| empenhado | 231.236,32 | Valor total empenhado. |
| aliquidar | 73.401,53 | Valor a liquidar. |
| liquidado | 0,00 | Valor liquidado até o momento. |
| pago | 157.834,79 | Valor pago até o momento. |
| rpinscrito | 73.401,53 | Valor de restos a pagar inscritos. |
| rpaliquidar | 62.408,78 | Valor de restos a pagar a liquidar. |
| rpliquidado | 62.408,78 | Valor de restos a pagar já liquidado. |
| rppago | 10.992,75 | Valor de restos a pagar já pago. |
| informacao_complementar | 11360106000072022 - UASG MINUTA: 113601 | Informações adicionais relacionadas ao empenho. |
| sistema_origem | COMPRASNET CONTRATOS | Sistema de origem do registro do empenho. |
| links.documento_pagamento | https://sta.api.gov.br/api/ordembancaria/empenho/113601113022023NE000139 | Link para o documento de pagamento associado. |
| credor_obj.tipo | JURIDICA | Tipo do credor (Pessoa Jurídica ou Física). |
| credor_obj.cnpj_cpf_idgener | 07.522.669/0001-92 | CNPJ ou CPF do credor. |
| credor_obj.nome | NEOENERGIA DISTRIBUICAO BRASILIA S.A. | Nome do credor. |

## Siafi
**Forma de extração:** Tesouro Gerencial a partir de relatórios programados e enviados em CSV via email

### Empenhos (anual)
**Relatório:** `Consulta por Execução Emp liq e pago`
Retorna a lista de empenhos, separados por ano de emissão, das naturezas de despesa relacionadas aos contratos.

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 787 | Identificador único do registro, gerado na ingestão. |
| ne_ccor | 113601113022024NE000059 | Número do empenho no Siafi. |
| ne_informacao_complementar | 11360105000112022 - UASG MINUTA: 113601 | Informações complementares do empenho. |
| ne_num_processo | 03001.000001/2024-18 | Número do processo relacionado ao empenho. |
| ne_ccor_descricao | CONTRATACAO DE EMPRESA ESPECIALIZADA NA PRESTACAO DE SERVICOS DE AGENCIAMENTO DE VIAGENS - INTERNACIONAL - CT. 11/2022 - DIEST | Descrição do objeto do empenho. |
| doc_observacao | CONTRATACAO DE EMPRESA ESPECIALIZADA NA PRESTACAO DE SERVICOS DE AGENCIAMENTO DE VIAGENS - INTERNACIONAL - CT. 11/2022 - DIEST | Observações relacionadas ao documento do empenho. |
| natureza_despesa | 339033 | Código da natureza de despesa. |
| natureza_despesa_1 | PASSAGENS E DESPESAS COM LOCOMOCAO | Descrição da natureza de despesa. |
| natureza_despesa_detalhada | 33903302 | Código da natureza de despesa detalhada. |
| natureza_despesa_detalhada_1 | PASSAGENS PARA O EXTERIOR | Descrição da natureza de despesa detalhada. |
| ne_ccor_favorecido | 06064175000149 | CNPJ ou CPF do favorecido. |
| ne_ccor_favorecido_1 | AIRES TURISMO LTDA | Nome do favorecido. |
| ne_ccor_ano_emissao | 2024 | Ano de emissão do empenho. |
| item_informacao | 2024 | Ano de emissão do empenho. |
| despesas_empenhadas_saldo | 10.000,00 | Saldo acumulado empenhado. Valores entre parenteses representam saldo negativo. |
| despesas_empenhadas_movim_liquido | 10.000,00 | Movimento líquido empenhado. Valores entre parenteses representam movimento negativo. |
| despesas_liquidadas_saldo | 10.000,00 | Saldo acumulado liquidado. Valores entre parenteses representam saldo negativo. |
| despesas_liquidadas_movim_liquido | 10.000,00 | Movimento líquido liquidado. Valores entre parenteses representam movimento negativo. |
| despesas_pagas_saldo | 10.000,00 | Saldo acumulado pago. Valores entre parenteses representam saldo negativo. |
| despesas_pagas_movim_liquido | 10.000,00 | Movimento líquido pago. Valores entre parenteses representam movimento negativo. |

### Empenhos (mensal)
**Relatório:** `Consulta por Execução Emp liq e pago (mensal)`
Retorna a lista de empenhos, separados por mês de emissão, das naturezas de despesa relacionadas aos contratos.

| Variável | Exemplo | Descrição |
| --- | --- | --- |
| id | 787 | Identificador único do registro, gerado na ingestão. |
| ne_ccor | 113601113022024NE000059 | Número do empenho no Siafi. |
| ne_informacao_complementar | 11360105000112022 - UASG MINUTA: 113601 | Informações complementares do empenho. |
| ne_num_processo | 03001.000001/2024-18 | Número do processo relacionado ao empenho. |
| ne_ccor_descricao | CONTRATACAO DE EMPRESA ESPECIALIZADA NA PRESTACAO DE SERVICOS DE AGENCIAMENTO DE VIAGENS - INTERNACIONAL - CT. 11/2022 - DIEST | Descrição do objeto do empenho. |
| doc_observacao | CONTRATACAO DE EMPRESA ESPECIALIZADA NA PRESTACAO DE SERVICOS DE AGENCIAMENTO DE VIAGENS - INTERNACIONAL - CT. 11/2022 - DIEST | Observações relacionadas ao documento do empenho. |
| natureza_despesa | 339033 | Código da natureza de despesa. |
| natureza_despesa_1 | PASSAGENS E DESPESAS COM LOCOMOCAO | Descrição da natureza de despesa. |
| natureza_despesa_detalhada | 33903302 | Código da natureza de despesa detalhada. |
| natureza_despesa_detalhada_1 | PASSAGENS PARA O EXTERIOR | Descrição da natureza de despesa detalhada. |
| ne_ccor_favorecido | 06064175000149 | CNPJ ou CPF do favorecido. |
| ne_ccor_favorecido_1 | AIRES TURISMO LTDA | Nome do favorecido. |
| ano_lancamento | 2024 | Ano de lançamento do empenho. |
| ne_ccor_mes_emissao | 07 | Mês de emissão do empenho. |
| ne_ccor_ano_emissao | 2024 | Ano de emissão do empenho. |
| item_informacao | 2024 | Mês de emissão do empenho. |
| despesas_empenhadas_saldo | 10.000,00 | Saldo acumulado empenhado. Valores entre parenteses representam saldo negativo. |
| despesas_empenhadas_movim_liquido | 10.000,00 | Movimento líquido empenhado. Valores entre parenteses representam movimento negativo. |
| despesas_liquidadas_saldo | 10.000,00 | Saldo acumulado liquidado. Valores entre parenteses representam saldo negativo. |
| despesas_liquidadas_movim_liquido | 10.000,00 | Movimento líquido liquidado. Valores entre parenteses representam movimento negativo. |
| despesas_pagas_saldo | 10.000,00 | Saldo acumulado pago. Valores entre parenteses representam saldo negativo. |
| despesas_pagas_movim_liquido | 10.000,00 | Movimento líquido pago. Valores entre parenteses representam movimento negativo. |

## Tabelas Silver
### Empenhos (Siafi) vs Contratos (ComprasGov)
