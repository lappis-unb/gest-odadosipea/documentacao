# POC da solução de software para extração automatizada de dados e visualização da informação

Relatório de execução financeira do contrato de água.

## Contexto
### Objetivo:
- Realizar acompanhamento financeiro da execução do contrato
- Cruzar valores estimados e valores executados
- Ter visão real do quanto foi gasto e quanto sobrou ou faltou

### Público alvo:
- Presidência/Diretoria
- Público externo
- Coordenação de serviços gerais e apoio a pesquisa

### Escopo:
- Orçamento estimado para o contrato no PNCP
- Orçamento/financeiro executado para o contrato no SIAFI

### Principais perguntas de negócio:
- Qual o limite para gastos excedentes nesse contrato?
- Quanto posso cortar do orçamento desse contrato para alocar para outros gastos? (contingenciamento)
- Como posso melhorar o planejamento dos contratos?

## Fonte dos dados
### Endpoints/APIs:
https://api.compras.dados.gov.br/

### Frequência:
Mensal

## Elementos de dados e métricas
### Indicadores:
#### Acompanhamento mensal
   - mês
   - valor previsto
   - valor realizado
   - saldo

#### Acompanhamento geral
   - valor total previsto
   - valor total realizado
   - saldo total

#### Visualização do orçamento de todos os contratos do ano atual
  - Valor total estimado dos contratos (somatório do valor anual dos contratos)
  - Quantidade de contratos
  - Listagem dos contratos: objeto, vigência (data inicio/fim), valor anual do contrato, valor global do contrato, quantidade de parcelas
  - Classificação dos contratos em: serviços, materiais, serviços de TI, obras e serviços de engenharia.
  - Classificação dos contratos em: pagamento único ou recorrentes (mais de uma parcela)

#### Visualização da força de trabalho de terceirizados
   - Informações do contrato de terceirização
   - Quantidade de colaboradores terceirizados
   - Custo total dos colaboradores terceirizados
   - Classificação dos terceirizados em: função, unidade, escolaridade
   - Custo total dos colaboradores terceirizados por unidade
   - Quantidade de terceirizados por unidade
   - Quantidade de terceirizados

#### Execução das ordens de serviço
  - valor total previsto
  - valor total executado

#### Execução

### Métodos de cálculo/classificação:
   - saldo: valor previsto - valor realizado 
   - saldo total: valor total previsto - valor toral realizado
   - valor total estimado dos contratos: somatório do valor anual dos contratos

<!-- ### Agregadores:
--

### Filtros:
#### Acompanhamento mensal
- mês
- processo

#### Acompanhamento geral
- intervalo de data

## Layout
### Protótipo:
   1. Organização e formatação dos dados
   2. Tipo de gráfico

## Entrega e manutenção
### Controle de acesso
Aberto a todos os usuários internos e externos

### Trilha de auditoria
--

### Documentação: -->



